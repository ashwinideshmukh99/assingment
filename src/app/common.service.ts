import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  public source = new BehaviorSubject<any>(' ');
  constructor() { }

  data = this.source.asObservable();
  update(values: any) {
    this.source.next(values);
  }

}
