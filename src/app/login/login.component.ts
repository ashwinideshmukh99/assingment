import { Component, OnInit,ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/common.service';
import { ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChild("confirmModal") public confirmModal: ModalDirective;
  public userName;userPassword;
  constructor(public router: Router,public dataSvc: CommonService,private route: ActivatedRoute) { }
  data: any;

  ngOnInit(): void {
  }

  saveData(){
    var data={}
    data['first_name']=this.userName;
    data['last_name']='Dummyname';
    data['exp']=3;
    window.localStorage.setItem('userInfo',JSON.stringify(data))
    this.dataSvc.update(JSON.parse(window.localStorage.getItem('userInfo')));
  }

  login(){
    this.confirmModal.hide()
  	console.log("LOGIN")
    setTimeout(()=>{
      this.router.navigate(['/details/personal']);
    }, 500);
  }

}
