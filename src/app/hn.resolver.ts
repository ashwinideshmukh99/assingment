import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Resolve } from '@angular/router';

import { Observable } from 'rxjs';

@Injectable()
export class HnResolver implements Resolve<any> {
  constructor(public router: Router) {}

  resolve() {
  console.log("Res ",window.localStorage)
	if(window.localStorage==null  || window.localStorage==undefined || window.localStorage.length==0){
	  this.router.navigate(['/']);
	  alert('No Data Available')
      return false;
    }else{
      return true;
    }  }
}