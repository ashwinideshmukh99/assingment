import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonalComponent } from './personal/personal.component';
import { SidepanelComponent } from './sidepanel/sidepanel.component';
import { ProffesionalComponent } from './proffesional/proffesional.component';

const routes: Routes = [
  { 
  	path: "personal",
    component: PersonalComponent,
  },
  { 
  	path: "proffesional",
    component: ProffesionalComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
