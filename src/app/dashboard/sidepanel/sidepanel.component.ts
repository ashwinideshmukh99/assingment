import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/common.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-sidepanel',
  templateUrl: './sidepanel.component.html',
  styleUrls: ['./sidepanel.component.css']
})
export class SidepanelComponent implements OnInit {
public fullName;subscription: Subscription;

  constructor(public dataSvc: CommonService,public router: Router) { 
   this.subscription = this.dataSvc.data.subscribe(
        data => {console.log('Data:', data)
          this.fullName=data['first_name']+' '+data['last_name']
        },
        err => console.log(err),
        () => console.log('complete')
    );
  }

  ngOnInit(): void {
  }

  goToPersonal(){
  	this.dataSvc.update(JSON.parse(window.localStorage.getItem('userInfo')));
  	this.router.navigate(['/details/personal']);
  }
  goToProf(){
  	this.dataSvc.update(JSON.parse(window.localStorage.getItem('userInfo')));
  	this.router.navigate(['/details/proffesional']);
  }

  logout()
  {
    this.router.navigate(['/']);
    window.localStorage.clear()
  }

}
