import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/common.service';

@Component({
  selector: 'app-proffesional',
  templateUrl: './proffesional.component.html',
  styleUrls: ['./proffesional.component.css']
})
export class ProffesionalComponent implements OnInit {

  public experience;data;
  constructor(public dataSvc: CommonService) { }

  ngOnInit(): void {
    this.data=JSON.parse(window.localStorage.getItem('userInfo'))
    this.experience=this.data['exp']
  }

  save(){
    var data={}
    data['first_name']=this.data['first_name']
    data['last_name']=this.data['last_name']
    data['exp']=this.experience;
    this.dataSvc.update(data);
    window.localStorage.setItem('userInfo',JSON.stringify(data))
    console.log("SAVE")
  }
  cancle(){
    var dummydata=JSON.parse(window.localStorage.getItem('userInfo'))
    this.experience=dummydata['exp']
  }
}
