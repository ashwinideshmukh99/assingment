import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/common.service';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit {

  public firstName;lastName;data;
  constructor(public dataSvc: CommonService) { }

  ngOnInit(): void {
  	this.data=JSON.parse(window.localStorage.getItem('userInfo'))
  	this.firstName=this.data['first_name']
  	this.lastName=this.data['last_name']
  }

  save(){
   	var data={}
    data['first_name']=this.firstName;
    data['last_name']=this.lastName;
    data['exp']=this.data['exp'];
    this.dataSvc.update(data);

    window.localStorage.setItem('userInfo',JSON.stringify(data))
    console.log("SAVE")
  }

  cancle(){
    var dummydata=JSON.parse(window.localStorage.getItem('userInfo'))
  	this.firstName=dummydata['first_name']
  	this.lastName=dummydata['last_name']
  }
}
