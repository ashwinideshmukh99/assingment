import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { PersonalComponent } from './personal/personal.component';
import { SidepanelComponent } from './sidepanel/sidepanel.component';
import { ProffesionalComponent } from './proffesional/proffesional.component';

@NgModule({
  declarations: [PersonalComponent, SidepanelComponent, ProffesionalComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule
  ]
})
export class DashboardModule { }
