import { Component,OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	constructor(public router: Router){}

	public loginFlag=false;
  	gotoLogin(){
  		this.loginFlag=true;
  		this.router.navigate(['/login']);
	}
}
