import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {DashboardModule}from './dashboard/dashboard.module';
import { HnResolver } from './hn.resolver';

const routes: Routes = [
  {path: 'login', component: LoginComponent },
  {path: '', component: LoginComponent },
  {
    path: 'details',
    loadChildren: './dashboard/dashboard.module#DashboardModule',
    resolve: { message: HnResolver }
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
